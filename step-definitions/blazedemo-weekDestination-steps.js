module.exports = function () {
    this.Then(/^I'm going to check if there at least (\d+) destination of the week$/, function (arg1) {
        // Write code here that turns the phrase above into concrete actions
        return page.blazedemo.findDestinationOfTWeek();
    });
    this.Then(/^I should see a picture of (\d+) destination$/, function (arg1) {
        // Write code here that turns the phrase above into concrete actions
        // driver wait returns a promise so return that
        return driver.wait(until.elementsLocated(by.tagName('img')), 10000).then(function(){
            // return the promise of an element to the following then.
            return driver.findElements(by.tagName('img'));
        })
        .then(function (elements) {
            expect(elements.length).to.equal(parseInt(arg1));
        });
    });
};