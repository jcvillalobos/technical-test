module.exports = function () {

    this.Then(/^I navigate to Blazedemo i should see a welcome message to "([^"]*)"$/, function (welcomeMessage) {
        // Write code here that turns the phrase above into concrete actions
        return helpers.loadPage('http://www.blazedemo.com').then(function() {
            // use a method on the page object which also returns a promise
            return page.blazedemo.findWelcomeMessage(welcomeMessage);
        })
    });
    this.Then(/^I'm going to search for flights from "([^"]*)" to "([^"]*)"$/, function (arg1, arg2) {
        // Write code here that turns the phrase above into concrete actions
        return page.blazedemo.findFlights(arg1, arg2);
        // return helpers.loadPage('http://www.blazedemo.com').then(function() {
        //     // use a method on the page object which also returns a promise
        //     return page.blazedemo.findFlights(arg1, arg2);
        // })
    });
    this.Then(/^I should see flights from "([^"]*)" to "([^"]*)"$/, function (arg1, arg2) {
        // Write code here that turns the phrase above into concrete actions
        // driver wait returns a promise so return that
        return driver.wait(until.elementsLocated(by.tagName('h3')), 10000).then(function(){
            // return the promise of an element to the following then.
            return driver.findElement(by.tagName('h3'));
        })
        .then(function (element) {
            element.getText().then(function(text) {
                // verify this element has children
                expect(text).to.equal('Flights from ' + arg1 + ' to ' + arg2 + ':');
            });
            
        });
    });
};