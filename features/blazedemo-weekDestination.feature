Feature: Check if there is a destination of the week available
  As an internet user
  In order to book a flight 
  I want to be able to find the destination of the week
 
  Scenario: Check Blazedemo for the destination of the week
    When I navigate to Blazedemo i should see a welcome message to "Simple Travel Agency!"
    Then I'm going to check if there at least 1 destination of the week
    Then I should see a picture of 1 destination

  Scenario: Check Blazedemo for the destination of the week
    When I navigate to Blazedemo i should see a welcome message to "Simple Travel Agency!"
    Then I'm going to check if there at least 2 destination of the week
    Then I should see a picture of 2 destination