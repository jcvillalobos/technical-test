Feature: Checking flights from Philadelphia to Rome
  As an internet user
  In order to book a flight 
  I want to be able to find information of all the flights to Rome
 
  Scenario: Check Blazedemo for flights from Philadelphia to Rome
    When I navigate to Blazedemo i should see a welcome message to "Simple Travel Agency!"
    Then I'm going to search for flights from "Philadelphia" to "Rome"
    Then I should see flights from "Philadelphia" to "Rome"
    
  Scenario: Check Blazedemo for flights from Filadelfia to Roma
    When I navigate to Blazedemo i should see a welcome message to "Simple Travel Agency!"
    Then I'm going to search for flights from "Filadelfia" to "Roma"
    Then I should see flights from "Filadelfia" to "Roma"