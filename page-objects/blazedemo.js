module.exports = {
 
    elements: {
        departureCity: by.name('fromPort'),
        destinationCity: by.name('toPort'),
        searchInput: by.className('btn btn-primary'),
        welcomeMessage: by.tagName('h1'),
        anchorDestionationLink: by.linkText('destination of the week! The Beach!'),
    },
    /**
     * enters departure and destination city
     * @param {string} welcomeMessage 
     * @returns {Promise} a promise to enter the search values
     */
    findWelcomeMessage: function (welcomeMessage) {
        return driver.wait(until.elementsLocated(by.tagName('h1')), 10000).then(function(){
            // return the promise of an element to the following then.
            return driver.findElement(by.tagName('h1'));
        })
        .then(function (element) {
            element.getText().then(function(text) {
                // verify this element has children
                return expect(text).to.equal('Welcome to the ' + welcomeMessage);
            });
        });
    },
    /**
     * enters departure and destination city
     * @param {string} departureCity 
     * @param {string} destinationCity 
     * @returns {Promise} a promise to enter the search values
     */
    findFlights: function (departureCity, destinationCity) {

        this.departureCity(departureCity);
        this.destinationCity(destinationCity);

        var selector = page.blazedemo.elements.searchInput;
 
        // return a promise so the calling function knows the task has completed
        return driver.findElement(selector).click();
    },
    /**
     * enters a departure city into Blazedemo input and presses enter
     * @param {string} departureCity 
     * @returns {Promise} a promise to enter the search values
     */
    departureCity: function (departureCity) {

        var selector = page.blazedemo.elements.departureCity;
 
        // return a promise so the calling function knows the task has completed
        return driver.findElement(selector).sendKeys(departureCity, selenium.Key.ENTER);
    },
    /**
     * enters a destination city into Blazedemo input and presses enter
     * @param {string} destinationCity 
     * @returns {Promise} a promise to enter the search values
     */
    destinationCity: function (destinationCity) {

        var selector = page.blazedemo.elements.destinationCity;
 
        // return a promise so the calling function knows the task has completed
        return driver.findElement(selector).sendKeys(destinationCity, selenium.Key.ENTER);
    },
    /**
     * selects the link to destionation of the week
     * @returns {Promise} a promise to enter the search values
     */
    findDestinationOfTWeek: function () {
        return driver.wait(until.elementsLocated(page.blazedemo.elements.anchorDestionationLink), 10000).then(function(){
            // return the promise of an element to the following then.
            return driver.findElement(page.blazedemo.elements.anchorDestionationLink).click();
        });
        // .then(function (element) {
        //     element.sendKeys(Keys.ENTER);
        // });
    }
};